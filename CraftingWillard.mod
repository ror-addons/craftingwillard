<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="CraftingWillard" version="2.9.0" date="27/12/2008" >
		<Author name="Onni" email="" />
		<Description text="(version 2.9.0 2009-07-16) Shows info about crafting resource values." />
    <VersionSettings gameVersion="1.3.0" windowsVersion="1.0" savedVariablesVersion="1.0" />		
		<Dependencies>
			<Dependency name="EASystem_Utils" />
			<Dependency name="EASystem_Tooltips" />
			<Dependency name="EA_ChatWindow" />
			<Dependency name="EA_CraftingSystem" />
			<Dependency name="EA_CultivationWindow" />
			<Dependency name="EA_BackpackWindow" />
			<Dependency name="EASystem_LayoutEditor" />			
<!-- Optional dependency for LibSlash. This is initialized within lua. -->
<!-- Optional dependency for TalismanWillard. This is initialized within lua. -->
		</Dependencies>
		<Files>
			<File name="CraftingWillard.lua" />
			<File name="CraftingWillard.xml" />
		</Files>
		<SavedVariables>
			<SavedVariable name="CraftingWillardSavedVariables" />
		</SavedVariables>
		<OnInitialize>
			<CreateWindow name="CraftingWillardAnchor" />		
			<CallFunction name="CraftingWillard.OnInitialize" />
		</OnInitialize>
		<OnUpdate/>
		<OnShutdown/>
	</UiMod>
</ModuleFile>
