--[[
	This addon is for Warhammer Online. It adds component value window next to
	default Talismanmaking and Apothecary window. You can also list these values
	from your	backpack, bank and auction results using slash command.
	
	Supports automatic resource item data and crafting results collection.
	This data can then be exported to a file.
--]]

----------[Global variables]----------------------------------------------
local GG = {}
CraftingWillard = GG		--Global package

GG.debugMode = false
GG.debugLevel = 0

do --init vars
GG.T = {}		--All global tables
GG.H1 = {}	--Hooks: original function
GG.H2 = {}	--Hooks: new function
GG.D = {}		--Debug workflow
GG.C = {}		--Constants
--GG.S = {}		--Savedvariables reference, initialized elsewhere

GG.C.package = "CraftingWillard"
GG.C.apot = GameData.TradeSkills.APOTHECARY
GG.C.tali = GameData.TradeSkills.TALISMAN
GG.C.apotNew = GameData.TradeSkills.APOTHECARY + 100
GG.C.taliNew = GameData.TradeSkills.TALISMAN + 100
GG.C.apotTip1 = GameData.TradeSkills.APOTHECARY + 200
GG.C.apotTip2 = GameData.TradeSkills.APOTHECARY + 210
GG.C.taliTip1 = GameData.TradeSkills.TALISMAN + 200
GG.C.taliTip2 = GameData.TradeSkills.TALISMAN + 210
GG.C.WinName = {}
GG.C.WinName[GG.C.apot] = "ApothecaryWindow"
GG.C.WinName[GG.C.tali] = "TalismanMakingWindow"
GG.C.WinName[GG.C.apotNew] = GG.C.package..GG.C.apot
GG.C.WinName[GG.C.taliNew] = GG.C.package..GG.C.tali
GG.C.WinName[GG.C.apotTip1] = GG.C.package.."Tooltip"..GG.C.apotTip1
GG.C.WinName[GG.C.taliTip1] = GG.C.package.."Tooltip"..GG.C.taliTip1
GG.C.WinName[GG.C.apotTip2] = GG.C.package.."Tooltip"..GG.C.apotTip2
GG.C.WinName[GG.C.taliTip2] = GG.C.package.."Tooltip"..GG.C.taliTip2
GG.C.WinName["anchor"] = "CraftingWillardAnchor"
GG.C.sourceBack = 1 --backpack
GG.C.sourceBank = 2 --bank
GG.C.sourceAuct = 3 --auction house
GG.C.sourceData = 4 --database
GG.C.sourceCraf = 5 --backpacks crafting tab (since patch 1.3) --v1.3
GG.C.sourceNum = 5 --number of sources
GG.C.BackpackSourceInventory = 2 --v1.3
GG.C.BackpackSourceCrafting = 4 --v1.3
GG.C.eitemFilename = GG.C.package.."Items.txt"
GG.C.eresuFilename = GG.C.package.."Results.txt"
GG.C.tooltipTemplateId = 11919	--Book of binding
GG.C.resultCol = 6

GG.defaultOrder = "1,2,3,4,5"
GG.doDataExport = false
GG.doListExport = false
GG.T["Current"] = {}	--Totals for current selection
GG.T["Custom"] = {}	--Totals for custom selection

end --do --init vars
----------[Initialization]----------------------------------------------
function GG.OnInitialize()
	--SavedVariables
  if not CraftingWillardSavedVariables then	GG.Reset() end
	GG.S = CraftingWillardSavedVariables
	
	--Conversion tables
	GG.SetConversionTables()
	
	--Used to tell crafting item result.
  RegisterEventHandler(SystemData.Events.PLAYER_INVENTORY_SLOT_UPDATED , GG.C.package..".PLAYER_INVENTORY_SLOT_UPDATED")
	
	--Hooks
	GG.SetHooks()

	--Invisible anchor-point for custom window placement
	LayoutEditor.RegisterWindow(GG.C.WinName["anchor"], L"CraftingWillard Anchor", L"For custom CraftingWillard-window placement.", false, false, true, nil)
    
	--Optional dependencies
	if GG.OptionalDependencies("LibSlash") then
		if LibSlash then
			LibSlash.RegisterWSlashCmd("cwillard", function(args) GG.SlashCmd(args) end)
			LibSlash.RegisterWSlashCmd("CraftingWillard", function(args) GG.SlashCmd(args) end)
		end
		EA_ChatWindow.Print(L"CraftingWillard-addon loaded. Command /cwillard enabled.")
	else
		EA_ChatWindow.Print(L"CraftingWillard-addon loaded.")
	end
	if GG.OptionalDependencies("TalismanWillard") then
		if TalismanWillard then
			EA_ChatWindow.Print(L"Warning: You have both TalismanWillard and CraftingWillard installed.")
			EA_ChatWindow.Print(L"Please uninstall TalismanWillard because it's no longer needed.")
		end
	end
end

function GG.InitWindows()
	if GG.tradeskill == CraftingSystem["currentTradeSkill"] then return end
	GG.SetParameters(CraftingSystem["currentTradeSkill"])
	GG.T.Labels = GG.SetLabelTemplate()
	GG.T.Editbox = GG.SetEditbox()
	if not DoesWindowExist(GG.winNew) then
		GG.CreateWindow(GG.winNew, GG.winTip1, GG.winTip2, GG.winMain, 500, {GG.rowsMax, 30}, 2*10)
		GG.CreateLabels(GG.rowsMax, GG.winNew.."Frame", GG.T.Labels)
	end
	GG.EditboxRefresh(nil, nil, "noupdate")
end

function GG.SetWindowLayer(change)
	local _default = Window.Layers.SECONDARY
	GG.S.customLayer = GG.S.customLayer or _default
	if change == "+" then GG.S.customLayer = GG.S.customLayer + 1
	elseif change == "-" then GG.S.customLayer = GG.S.customLayer - 1
	else GG.S.customLayer = _default
	end
	if GG.S.customLayer < Window.Layers.BACKGROUND then GG.S.customLayer = Window.Layers.BACKGROUND end
	if GG.S.customLayer > Window.Layers.OVERLAY then GG.S.customLayer = Window.Layers.OVERLAY end
	return GG.S.customLayer
end

function GG.UpdateWindows()
	DestroyWindow(GG.winNew)
	DestroyWindow(GG.winTip1)
	DestroyWindow(GG.winTip2)
	GG.tradeskill = nil
	GG.InitWindows()
	GG.ShowWindows()
	GG.HelperWindowInfo()
end
----------[Slash command]----------------------------------------------
function GG.SlashCmd(args)
	local _Args = {}
	for i=1, 5 do
		if not args then break end
		_Args[i], args = args:match(L"([%w,+-]+)[ ]?(.*)")
	end
	local _num = tonumber(tostring(_Args[1])) or 0
	if _Args[1] == L"edata" then
		GG.doDataExport = true
		GG.doListExport = false
		EA_ChatWindow.Print(towstring("Next list command (all data) will be exported to file..."))
	elseif _Args[1] == L"eitem" then
		GG.doDataExport = false
		GG.doListExport = true
		EA_ChatWindow.Print(towstring("Next list command will be exported to file..."))
	elseif _Args[1] == L"eresu" then
		local _total = GG.ExportResults()
		EA_ChatWindow.Print(towstring("Exported ".._total.." results to file: /logs/"..GG.C.eresuFilename))
	elseif _Args[1] == L"titem" then
		GG.S.collectItems = not GG.S.collectItems
		EA_ChatWindow.Print(towstring("Collecting item data = "..tostring(GG.S.collectItems)))
	elseif _Args[1] == L"tresu" then
		GG.S.collectResults = not GG.S.collectResults
		EA_ChatWindow.Print(towstring("Collecting results data = "..tostring(GG.S.collectResults)))
	elseif _Args[1] == L"anchor" and _Args[2] == L"toggle" then
		GG.S.customAnchor = not GG.S.customAnchor
		GG.UpdateWindows()
		EA_ChatWindow.Print(towstring("Custom window anchoring = "..tostring(GG.S.customAnchor)))
	elseif _Args[1] == L"layer" then
		local _layer = GG.SetWindowLayer(tostring(_Args[2]))
		GG.UpdateWindows()
		EA_ChatWindow.Print(towstring("Layer set to = ".._layer))
	elseif _Args[1] == L"testtooltip1" then
		GG.TestWindow(GG.winTip1, "Tooltip1")
	elseif _Args[1] == L"testtooltip2" then
		GG.TestWindow(GG.winTip2, "Tooltip2")
	elseif _Args[1] == L"7" then
		EA_ChatWindow.Print(towstring("missing..."))
	elseif _Args[1] == L"64" then
		EA_ChatWindow.Print(towstring("missing..."))
	elseif (_num > 0 and _num < 5) or _num == 9 then
		_Args[2] = _Args[2] or towstring(GG.defaultOrder)
		local _Source = {_num}
		if _num == 1 then	_Source = { 1, 5 } end --v1.3
		if _num == 4 then	_Source = { 1, 2, 5 } end --v1.3
		if _num == 9 then	_Source = { 4 } end
		local _List, _ArgFormat, _itemCount,
					_exportCount, _collectCount = GG.GetCraftingInfo(_Source, tostring(_Args[2]))
			
		GG.PrintList(_List, _ArgFormat, _itemCount)
		if _collectCount and _collectCount > 0 then
			EA_ChatWindow.Print(towstring("Collected ".._collectCount.." items to database"))
		end
		if _exportCount then
			EA_ChatWindow.Print(towstring("Exported ".._exportCount.." items to file: /logs/"..GG.C.eitemFilename))
		end
	else
		GG.SlashHelp(_Args[1])
	end
end

function GG.SlashHelp(str)
	local _Msg = {}
	table.insert(_Msg, "---CraftingWillard  slash command help ---")
	table.insert(_Msg, "/cwillard 1 => list all talisman components in your backpack")
	table.insert(_Msg, "/cwillard 2 => ...bank")
	table.insert(_Msg, "/cwillard 3 => ...last auction search")
	table.insert(_Msg, "/cwillard 4 => ...backpack and bank")
	table.insert(_Msg, "/cwillard 9 => ...database")
	table.insert(_Msg, "/cwillard edata => export all data for next item list to file")
	table.insert(_Msg, "/cwillard eitem => export next item list to file")
	table.insert(_Msg, "/cwillard eresu => export results to file")
	table.insert(_Msg, "/cwillard titem => toggle item data collection to database")
	table.insert(_Msg, "/cwillard tresu => toggle results data collection to database")
	table.insert(_Msg, "/cwillard anchor toggle => toggle custom anchor for help-windows")
	table.insert(_Msg, "/cwillard layer + => raise help-windows on top of other windows")
	table.insert(_Msg, "/cwillard layer - => lower help-windows on below other windows")
	table.insert(_Msg, "/cwillard help => long help about listing")
	
	if str and string.lower(tostring(str)) == "help" then
		table.insert(_Msg, "---CraftingWillard detailed listing help ---")
		table.insert(_Msg, "Default list will be in this format: 1,2,3,4,5")
		table.insert(_Msg, "1: resource type")
		table.insert(_Msg, "2: resource value")
		table.insert(_Msg, "3: resource stability value (for apothecary)")
		table.insert(_Msg, "4: SourceRow:Col (eg. bank => b1:8)")
		table.insert(_Msg, "5: item name")
		table.insert(_Msg, "")
		table.insert(_Msg, "You can set your own order and sorting with slash command: ")
		table.insert(_Msg, "/cwillard 1 5,1,2,3,4 => Item name is placed as first")
		table.insert(_Msg, "/cwillard 1 4,d => backpack position first then all default")
		table.insert(_Msg, "/cwillard 1 b5,d => required tradeskill first then all default")
		table.insert(_Msg, "/cwillard 1 1,2 => just resource type and value")
		table.insert(_Msg, "")
		table.insert(_Msg, "Here are all available extra options:")
		table.insert(_Msg, "d: all default options from Willard")
		table.insert(_Msg, "6: item rarity (from item)")
		table.insert(_Msg, "7: resource rarity bonus?? (from resource bonuses)")
		table.insert(_Msg, "8: all resource bonuses")
		table.insert(_Msg, "9: Item level?")
		table.insert(_Msg, "10: Item Id")
		table.insert(_Msg, "103: Talisman fragment result")
		table.insert(_Msg, "104: Apothecary main ingredient result")
		table.insert(_Msg, "b#: specific resource bonus (eg. b2 => resource value)")
	end
	for k, v in ipairs(_Msg) do
			EA_ChatWindow.Print(towstring(v))
	end
end

----------[Window functions]----------------------------------------------
function GG.CreateWindow(winNew, winTip1, winTip2, winParent, windth, Height, padding)
	local _layer = Window.Layers.SECONDARY
	if GG.S.customLayer then
		_layer = GG.S.customLayer
	end
	if GG.S.customAnchor then
		winParent = GG.C.WinName["anchor"]
		WindowSetDimensions(winParent, windth, 10)
	end

	CreateWindowFromTemplate(winNew, GG.C.package.."Main", "Root")
	WindowSetDimensions(winNew, windth, (Height[1]*Height[2]) + padding)
	WindowClearAnchors(winNew)
	WindowAddAnchor(winNew, "topright", winParent, "bottomright", 50, 0)
	WindowSetShowing(winNew, false)
	WindowSetLayer(winNew, _layer)

	CreateWindowFromTemplate(winTip1, "ItemTooltip", "Root")
	WindowClearAnchors(winTip1)
	WindowAddAnchor(winTip1, "bottomleft", winParent, "topleft", 0, 0)
	WindowSetShowing(winTip1, false)
	WindowSetLayer(winTip1, _layer)
	
	CreateWindowFromTemplate(winTip2, "ItemTooltip", "Root")
	WindowClearAnchors(winTip2)
	WindowAddAnchor(winTip2, "topright", winTip1, "topleft", 0, 0)
	WindowSetShowing(winTip2, false)
	WindowSetLayer(winTip2, _layer)
end

function GG.CreateLabels(numLabel, winParent, Rows)
	local _windth, _height = WindowGetDimensions(winParent)
	local _heightLabel = _height / numLabel
	GG.T.LabelMatrix = {}
	GG.Debug(1, Rows, "_Rows-CreateLabels")
	for iRow, vRow in ipairs(Rows) do
		local _y1 = _heightLabel * iRow - _heightLabel
		local _y2 = _heightLabel * iRow
		local _windthCounter = 0
		local _NameCounters = {}
		for kCol, vCol in ipairs(vRow) do
			local _x1 = _windthCounter
			_windthCounter = _windth / (100 / vCol[1]) + _windthCounter
			local	_x2 = _windthCounter
			_NameCounters[vCol[3]] = _NameCounters[vCol[3]] or 0
			_NameCounters[vCol[3]] = _NameCounters[vCol[3]] + 1
			local _winName = winParent.."Row"..iRow..vCol[3].._NameCounters[vCol[3]]
			if not DoesWindowExist(_winName) then
				GG.T.LabelMatrix[iRow] = GG.T.LabelMatrix[iRow] or {}
				GG.T.LabelMatrix[iRow][kCol] = {_winName, vCol[3], _NameCounters[vCol[3]]}
				CreateWindowFromTemplate(_winName, GG.C.package..vCol[2], winParent)
				if DoesWindowExist(_winName.."Background") then
					WindowSetAlpha(_winName.."Background", 0)
				end
			end
			WindowClearAnchors(_winName)
			WindowAddAnchor(_winName, "topleft", winParent, "topleft", _x1, _y1)
			WindowAddAnchor(_winName, "topleft", winParent, "bottomright", _x2, _y2)
		end
	end
end


function GG.ShowWindows()
	GG.InitWindows()
	WindowSetShowing(GG.winNew, WindowGetShowing(GG.winMain))
	if not WindowGetShowing(GG.winMain) then 
		WindowSetShowing(GG.winTip1, false)
		WindowSetShowing(GG.winTip2, false)
	end

	if TalismanWillard and DoesWindowExist("TalismanWillardWindow") then
		WindowSetDimensions("TalismanWillardWindow", 10, 10)
		WindowSetShowing("TalismanWillardWindow", false)
	end
end
----------[Window eventhandler functions]----------------------------------------------
function GG.OnLButtonUp(flags)
	GG.OnButtonUp("left")
end

function GG.OnRButtonUp(flags)
	GG.OnButtonUp("right")
end

function GG.OnButtonUp(mouse)
	local _row = tonumber(string.match(SystemData.MouseOverWindow.name, ".+Row(%d+)"))
	--Minus button
  if string.find(SystemData.MouseOverWindow.name, "Minus") then
		GG.EditboxRefresh(_row, mouse, "minus")
	end
	--Plus button
  if string.find(SystemData.MouseOverWindow.name, "Plus") then
		GG.EditboxRefresh(_row, mouse, "plus")
	end
	--Editbox
  if string.find(SystemData.MouseOverWindow.name, "Editbox") then
		if mouse == "right" then
			if _row == 1 then
				for i=1, GG.rowsMax do
					GG.EditboxRefresh(i, mouse, "reset")
					GG.EditboxRefresh(i, mouse, "set", GG.T["Current"][i])
				end
			else
				GG.EditboxRefresh(_row, mouse, "reset")
			end
		end
	end
	GG.EditboxRefresh()
end

function GG.OnKeyEnter(flags)
	GG.EditboxRefresh()
end

function GG.EditboxRefresh(row, mouse, action, value)
	local _min = row or 1
	local _max = row or GG.rowsMax
	local _step = 0
	if mouse == "left" then _step = GG.T.Editbox[row].step1 end
	if mouse == "right" then _step = GG.T.Editbox[row].step2 end
	if action == "minus" then _step = _step * -1 end
	if action == "reset" then _step = 9999 end
	if action == "set" then _step = value end
	for iRow=_min, _max do
		local _path = GG.winNew.."FrameRow"..iRow.."Counter1Editbox"
		local _number = tostring(_G[_path].Text)
		_number = tonumber(_number) or 0
		if mouse == "right" and action == "plus" and GG.tradeskill == GG.C.tali and iRow == 3 then
			_step = GG.GetTaliLevel(_number)
		end
		if mouse == "right" and action == "minus" and GG.tradeskill == GG.C.tali and iRow == 3  then
			_step = GG.GetTaliLevel(_number, "prev")
		end
		if mouse == "left" and action == "plus" and GG.tradeskill == GG.C.apot and iRow == 1 then
			_step = GG.GetApotLevel(_number)
		end
		if mouse == "left" and action == "minus" and GG.tradeskill == GG.C.apot and iRow == 1 then
			_step = GG.GetApotLevel(_number, "prev")
		end
		_number = _number + _step
		if _step == 9999 then _number = 0 end
		if _number < GG.T.Editbox[iRow].min then _number = GG.T.Editbox[iRow].min end
		if _number > GG.T.Editbox[iRow].max then _number = GG.T.Editbox[iRow].max end
		TextEditBoxSetText(_path, towstring(_number))
		GG.T["Custom"][iRow] = _number
	end
	if not noupdate then GG.HelperWindowInfo() end
end

----------[Result functions]----------------------------------------------
function GG.CreateTooltipTemplate()
	local _T = {}
--		_T = DataUtils.CopyTable(DataUtils.FindItem(GG.C.tooltipTemplateId))
	_T["description"] = L""
	_T["iLevel"] = 1
	_T["maxEquip"] = 0
	_T["bop"] = false
	_T["equipSlot"] = 0
	_T["tintB"] = 0
	_T["armor"] = 0
	_T["level"] = 1
	_T["blockRating"] = 0
	_T["iconNum"] = 0
	_T["rarity"] = 2
	_T["marketingVariation"] = 0
	_T["name"] = L"Empty"
	_T["broken"] = false
	_T["marketingIndex"] = 0
	_T["uniqueID"] = 0
	_T["sellPrice"] = 0
	_T["stackCount"] = 1
	_T["craftingSkillRequirement"] = 0
	_T["cultivationType"] = 0
	_T["dyeTintB"] = 0
	_T["currChargesRemaining"] = 0
	_T["dyeTintA"] = 0
	_T["tintA"] = 0
	_T["trophyLocation"] = 0
	_T["type"] = 0
	_T["boundToPlayer"] = false
	_T["numEnhancementSlots"] = 0
	_T["id"] = 0
	_T["dps"] = 0
	_T["trophyLocIndex"] = 0
	_T["itemSet"] = 0
	_T["renown"] = 0
	_T["noChargeLeftDontDelete"] = 0
	_T["isNew"] = false
	_T["isRefinable"] = false
	_T["speed"] = 0
	_T["races"] = {}
	_T["flags"] = {}
	for i=1, 15 do
		table.insert(_T["flags"], false)
	end
	_T["skills"] = {}
	_T["bonus"] = {}
	_T["bonus"][1] = {}
	_T["bonus"][1]["type"] = 0
	_T["bonus"][1]["value"] = 0
	_T["bonus"][1]["reference"] = 0
	_T["enhSlot"] = {}
	_T["craftingBonus"] = {}
	_T["careers"] = {}
	_T["slots"] = {}
  _T["timeLeftBeforeDecay"] = 0 --v1.3
  _T["timestamp"] = 0 --v1.3
  _T["decayPaused"] = false --v1.3
	GG.Debug(3, _T, "CreateTooltipTemplate")	
	return _T
end

function GG.ShowTooltip(itemData, winTip, winAnchor)
	GG.Debug(1, itemData, "itemData-ShowTooltip")
	GG.Debug(1, winTip, "winTip-ShowTooltip")
	GG.Debug(1, winParent, "winParent-ShowTooltip")
	if itemData then
		local _iLevel = "(hidden) itemLevel = "..itemData.iLevel
		if GG.tradeskill == GG.C.apot then
			Tooltips.SetItemTooltipData(winTip, itemData, towstring(_iLevel))
		else
			Tooltips.SetItemTooltipData(winTip, itemData)
		end
		WindowSetShowing(winTip, true)
		if winAnchor then 
			WindowClearAnchors(winTip)
			WindowAddAnchor(winTip, "topright", winAnchor, "topleft", 0, 0)
			WindowSetShowing(winTip, true)
		end
	else
		WindowSetDimensions(winTip, 135, 10)
		WindowSetShowing(winTip, false)
	end
end

function GG.CreateTooltipData(Result)
	if GG.tradeskill == GG.C.apot then return GG.CreateTooltipDataApot(Result)
	elseif GG.tradeskill == GG.C.tali then return GG.CreateTooltipDataTali(Result)
	end
end

function GG.CreateTooltipDataApot(Result)
	GG.Debug(1, Result, "List-CreateTooltipData")
	if not Result[1][6] or Result[1][6] == 0 then return end
--	if Result[2][6] < 1 then return end --not needed after patch 1.2
--	if Result[3][6] < 1 then return end --not needed after patch 1.2
--	if Result[4][6] < 1 then return end --not needed after patch 1.2
--	if Result[5][6] < 1 then return end --not needed after patch 1.2
	local _T = GG.CreateTooltipTemplate()

	local _type = Result[1][6]	--str, int, etc.
	local _volatile = (Result[5][6] == 1) --is it volatile
	
	local _mulLevel = GG.GetApotMultiplier(Result[4][6]) --multiplier
	local _durLevel = GG.GetApotDuration(Result[3][6]) --duration
	local _rank, _iLevel, _powLevel = GG.GetApotPower(Result[2][6], _volatile) --power level
  local _reference = GG.GetApotReference(_type, _powLevel, _durLevel, _volatile)

	_T["bonus"][1]["reference"] = _reference
	_T["iLevel"] = _iLevel
	_T["level"] = _rank
	if _type >= 21 then
		_T["iLevel"] = 50
		_T["level"] = 40
	end
	if _type >= 41 then
		_T["iLevel"] = 1
		_T["level"] = 1
	end
	if _type >= 45 then
		_T["iLevel"] = 20
		_T["level"] = 1
	end
	
	--Results: basic
	_T["name"] = towstring(GG.T.ApotDef[_type][3])
--	if _volatile and _type < 21 then _T["name"] = L"V-".._T["name"] end	--volatile
	if _volatile and _type < 21 then _T["description"] = L"VOLATILE" end	--volatile
	_T["iconNum"] = 416
	_T["bonus"][1]["type"] = 3
	_T["bonus"][1]["value"] = 1

	--Results: multiplier
	local _multiplier = ""
	if _mulLevel > 1 then _multiplier = " * ".._mulLevel end
	_T["name"] = _T["name"]..towstring(_multiplier)

	GG.Debug(1, _T, "CreateTooltipData")
	return _T
end

function GG.CreateTooltipDataTali(Result)
	GG.Debug(1, Result, "List-CreateTooltipData2")
	if not Result[1][6] or Result[1][6] == 0 then return end
	local _T = GG.CreateTooltipTemplate()
 	if Result[2][6] < 2 then return end
 	if Result[3][6] < 1 then return end

	local _type = Result[1][6]	--str, int, etc.
	local _rarity = Result[2][6]
	if _rarity > 5 then _rarity = 5 end
	
	--Results: basic
	_T["name"] = towstring(GG.T.TaliResult[_type]["name"])
	_T["rarity"] = _rarity
	_T["iconNum"] = 393
	_T["bonus"][1]["type"] = 2
	_T["bonus"][1]["reference"] = GG.T.TaliResult[_type]["link"]

	local _powTotal = 1	--power
  local _rarityPower = 0
	for k, v in ipairs(GG.T.TaliDef) do
--		if Result[3][6] <= v.max then _powTotal = _powTotal + k break end --v1.3
		if Result[3][6] <= v.max then _powTotal = k break end --v1.3
	end

  --level requirement
	if _type >= 49 then --not training talismans
    _T["level"] = 1
  else
    _T["level"] = GG.GetTaliLevelReq(_powTotal)
  end

  --duration
  _T["bonus"][1]["duration"] = GG.GetTaliDuration(_powTotal)
   
  --power
	if _type >= 1 and _type <= 4 then --armor
--		local _rarityPower = 0
		if _rarity > 2 then _rarityPower = 1 end
		_powTotal = GG.GetTaliArmorBonus(_powTotal, _rarityPower)
	elseif _type >= 5 and _type <= 16 then --resists
		if _rarity > 2 then _rarityPower = (_rarity - 2) * 15 end
    _powTotal = 5 + (_powTotal * 5)
		_powTotal = _powTotal + _rarityPower
	elseif _type >= 49 then --training
    _powTotal = 1
  else
		if _rarity > 2 then _rarityPower = (_rarity - 2) * 4 end
		_powTotal = 2 + _powTotal + _rarityPower
--[[ --v1.3
		if _rarity > 2 then _rarityPower = (_rarity - 2) * 3 end
		_powTotal = _powTotal + _rarityPower
		if _type >= 5 and _type <= 16 then _powTotal = _powTotal * 5 end --5 after patch 1.2
--]]
	end
	_T["bonus"][1]["value"] = _powTotal

--[[ --v1.3
	local _hour = 3600
	local _day = 24 * _hour
	local _duration = 0
	if _rarity <= 2 then _duration = 8 * _hour end
	if _rarity == 3 then _duration = 2 * _day end
	if _rarity == 4 then _duration = 5 * _day end
	if _type >= 49 then _duration = _hour / 2 end --training talisman
	_T["bonus"][1]["duration"] = _duration
--]]	

	GG.Debug(1, _T, "CreateTooltipDataTali")
	return _T
end
function GG.CreateTooltipComparison(Result, extra)
	local _T = DataUtils.CopyTable(Result)
	if GG.T["Custom"][1] == 0 then
		for i=1, GG.rowsMax do
			_T[i][6] = Result[i][6] + GG.T["Custom"][i]
		end
	else
		for i=1, GG.rowsMax do
			_T[i][6] = GG.T["Custom"][i]
		end
	end
	GG.Debug(1, _T, "CreateTooltipComparison")
	return _T
end

function GG.TestWindow(winName, title)
	if not winName then
		EA_ChatWindow.Print(towstring("Open your crafting window first..."))
		return
	end
	if not DoesWindowExist(winName) then
		EA_ChatWindow.Print(towstring(title.." exists: false"))
		return
	end
	EA_ChatWindow.Print(towstring(title.." exists: "..tostring(DoesWindowExist(winName))))
	EA_ChatWindow.Print(towstring(title.." is shown: "..tostring(WindowGetShowing(winName))))
	local _x, _y = WindowGetScreenPosition(winName)
	EA_ChatWindow.Print(towstring(title.." screen position X, Y: "..math.floor(_x)..", "..math.floor(_y)))
	local _x, _y = WindowGetDimensions(winName)
	EA_ChatWindow.Print(towstring(title.." size X, Y: "..math.floor(_x)..", "..math.floor(_y)))
end
----------[Crafting info functions]----------------------------------------------
--[[
	Parse the command line arguments to internal format. (GG.CreateArgFormat)
	Get item data from system. (GG.GetItemData)
	Change the table structures, but keep default values. (GG.GetItemValues)
	One item/slot can be used multiple times in crafting window. (if Slots)
		So add each slot individually to make copies if needed.
	Remove items based on default values. (GG.FilterItems)
	Get all requested data to new table. (GG.CreateList)
		Attn. The value list (sub-table) hasn't got all the keys anymore. (ipairs -> pairs)
		eg. _Itemlist[1] = {[1]=9, [3]=9, [4]=9}
	Change default values to internal format. (GG.DoListConversions)
--]]
function GG.GetCraftingInfo(Source, args, Slots)
	local _exportCount, _collectCount = nil, nil
	local _ArgFormat = GG.CreateArgFormat(args, GG.T.Formats)
	local _ItemData, _itemCount = GG.GetItemData(Source)
	local _ItemValues = GG.GetItemValues(_ItemData)
	if GG.S.collectItems then _collectCount = GG.SaveItems(_ItemData) end
	if GG.doDataExport then _exportCount = GG.ExportItems(_ItemValues) end

	local _ItemList = {}
	if Slots then
		for k, v in pairs(Slots) do
--			local _Item = GG.FilterItems(_ItemValues, "slot", {v}) --v1.3
			local _Item = GG.FilterItems(_ItemValues, "sSlot", {v})
			_Item = GG.CreateList(_Item, _ArgFormat)
			_Item = GG.DoListConversions(_Item, _ArgFormat)	
			_ItemList[k] = DataUtils.CopyTable(_Item[1])
		end
	else
		_ItemList = GG.CreateList(_ItemValues, _ArgFormat)
		_ItemList = GG.DoListConversions(_ItemList, _ArgFormat)
	end
	if GG.doListExport then _exportCount = GG.ExportItems(_ItemList) end
	GG.Debug(2, _ItemList, "CreateCraftingInfo")
	return _ItemList, _ArgFormat, _itemCount, _exportCount, _collectCount
end

function GG.HelperWindowInfo()
	GG.Debug(2, GameData["CraftingStatus"]["State"], "State-CraftingSystem_SetState")
	GG.Debug(2, GameData["CraftingStatus"]["ErrorCode"], "ErrorCode-CraftingSystem_SetState")
	--7 = perform crafting
	if GameData["CraftingStatus"]["State"] == 7 then 
			GG.T.Perform = {}
			GG.T.Perform["Status"] = {{}, {}}
			GG.T.Perform["Status"][1] = DataUtils.CopyTable(GameData.CraftingStatus)
			GG.T.Perform["Items"] = {{}, {}}
			for k, v in pairs(CraftingSystem["currentWindow"]["craftingData"]) do
				table.insert(GG.T.Perform["Items"][1], v.objectId)
			end
	end
	--4/5 = crafting succesful
	if GameData["CraftingStatus"]["State"] == 4
	or GameData["CraftingStatus"]["State"] == 5 then
		if GG.T.Perform then
			GG.T.Perform["Status"][2] = DataUtils.CopyTable(GameData.CraftingStatus)
			local _Item = DataUtils.GetItems()[GG.lastSlotUpdated]
			table.insert(GG.T.Perform["Items"][2], _Item.uniqueID)
			if GG.S.collectResults == true then
				table.insert(GG.S["Results"], DataUtils.CopyTable(GG.T.Perform))
			end
			GG.T.Perform = nil
		end
	end
	--6 = crafting failed
	if GameData["CraftingStatus"]["State"] == 6 then 
		if GG.T.Perform then
			GG.T.Perform["Status"][2] = DataUtils.CopyTable(GameData.CraftingStatus)
			if GG.S.collectResults == true then
				table.insert(GG.S["Results"], DataUtils.CopyTable(GG.T.Perform))
			end
			GG.T.Perform = nil
		end
	end
	--update helper window data
	local _Slots = GG.GetCraftingSlotsUsed(GG.winMain)
	local _List = {}
	if _Slots then
		_List = GG.GetCraftingInfo({GG.C.sourceBack, GG.C.sourceCraf}, GG.args, _Slots) --v1.3
	end
	GG.DoCalculations(_List)
end

function GG.CreateArgFormat(args, Format)
	local _T, _arg = {}, nil
	local _args = args
	local _defaultOrder = GG.defaultOrder
	
	while _args:len() > 0 do
		local _A = nil
		_arg, _args = _args:match("^(%w+),?(.*)$")
		if tonumber(_arg) and tonumber(_arg) > 0  then _arg = tonumber(_arg) end
		if type(_arg) == "number" and Format[_arg] then
			_A = DataUtils.CopyTable(Format[_arg])
		elseif _arg:match("^b(%d+)$") then _arg = tonumber(_arg:match("^b(%d+)$"))
		elseif _arg:match("^(d)$") then
			local _Default = GG.CreateArgFormat(_defaultOrder, Format)
			for k, v in ipairs(_Default) do
				table.insert(_T, v)
			end
			_arg = nil
		end
		if not _A and _arg then _A = {_arg, {}} end
		if _A then table.insert(_T, _A) end
	end
	GG.Debug(3, _T, "CreateArgFormat")
	return _T
end

function GG.DoCalculations(List)
	local _Result = GG.CreateCalculations(List)
	GG.UpdateCurrent(_Result)
	GG.Debug(1, _Result, "_Result-DoCalculations")
	
	GG.ShowCalculations(_Result)
	local _ItemData = GG.CreateTooltipData(_Result)
	GG.ShowTooltip(_ItemData, GG.winTip1)
	local _ResultComp = GG.CreateTooltipComparison(_Result)
	local _ItemData = GG.CreateTooltipData(_ResultComp)
	GG.ShowTooltip(_ItemData, GG.winTip2, GG.winTip1)
end
function GG.UpdateCurrent(Result)
	for i=1, GG.rowsMax do
		GG.T["Current"][i] = Result[i][GG.C.resultCol]
	end
end

function GG.GetCraftingSlotsUsed(CraftWindow)
	local Package = _G[CraftWindow]
	if not Package then return end
	if not Package["craftingData"] then return end
	local _T
	for i=0, 4 do
		if Package["craftingData"][i] then
			_T = _T or {}
      local _source = GG.C.sourceCraf --defaults to crafting tab
--			_T[i+1] = Package["craftingData"][i].sourceSlot --v1.3
      if Package["craftingData"][i].backpack == GG.C.BackpackSourceInventory then --.backpack in TM
        _source = GG.C.sourceBack
      end
      if Package["craftingData"][i].sourceBackpack == GG.C.BackpackSourceInventory then --.sourceBackpack in A
        _source = GG.C.sourceBack
      end
      if Package["craftingData"][i].sourceSlot then
        _T[i+1] = _source..Package["craftingData"][i].sourceSlot --v1.3
      end
		end
	end
	GG.Debug(3,_T, "GetCraftingSlotsUsed")
	return _T
end

function GG.ShowCalculations(Result)
	
	for kRow, vRow in pairs(GG.T.LabelMatrix) do --pairs
		for kCol, vCol in pairs(vRow) do --pairs
			local _winName = vCol[1]
			local _val = ""
			if vCol[2] == "Value" then
				_winName = _winName.."Text"
				_val = Result[kRow][vCol[3]] or 0
			elseif vCol[2] == "Total" or vCol[2] == "TotalB" then
				_winName = _winName.."Text"
				_val = Result[kRow][GG.C.resultCol] or 0
			elseif vCol[2] == "Equal" then _val = "="
			elseif vCol[2] == "Title" then
				_val = GG.T.LabelTitle[kRow]
				if kRow == GG.bonusRow then _val = _val.."(+"..GG.GetBonus()..")" end
				if kRow == GG.stabilityRow then _val = _val.."(+"..GG.GetBonusStability()..")" end
			elseif vCol[2] == "Rarity" and vCol[3] == 1 and Result[kRow][6] > 0 then
				_val = "("..Result[kRow][6]..") "..tostring(GameDefs.ItemRarity[Result[kRow][6]]["desc"])
			elseif vCol[2] == "Rarity" and vCol[3] == 2 and GG.T["Custom"][kRow] ~= 0 then
				local _rarity = GG.T["Custom"][kRow]
				if GG.T["Custom"][1] == 0 and Result[kRow][6] then 
					_rarity = _rarity + Result[kRow][6]
				end
				if _rarity < 1 then _rarity = 1 end
				if _rarity > 5 then _rarity = 5 end
				_val = "(".._rarity..") "..tostring(GameDefs.ItemRarity[_rarity]["desc"])
			elseif vCol[2] == "Counter" then continue
			elseif vCol[2] == "Type" then
				if vCol[3] == 1 and Result[kRow][2] then
					if GG.tradeskill == GG.C.apot then
						_val = "("..Result[kRow][6]..") "..GG.T.ApotDef[Result[kRow][6]][3]
					elseif GG.tradeskill == GG.C.tali then
						_val = "("..Result[kRow][6]..") "..GG.T.TaliResult[Result[kRow][6]]["name"]
					end
				elseif vCol[3] == 2 and GG.T["Custom"][kRow] > 0 then
					if GG.tradeskill == GG.C.apot then
						_val = "("..GG.T["Custom"][kRow]..") "..GG.T.ApotDef[GG.T["Custom"][kRow]][3]
					elseif GG.tradeskill == GG.C.tali then
						_val = "("..GG.T["Custom"][kRow]..") "..GG.T.TaliResult[GG.T["Custom"][kRow]]["name"]
					end
				end
			end
			if DoesWindowExist(_winName) then LabelSetText(_winName, towstring(_val)) end
			GG.SetBackgoundColor(vCol, _val, kRow)
		end
	end

end

function GG.SetBackgoundColor(Label, val, row)
	local _ColorPos = DefaultColor.TEAL
	local _ColorNeg = DefaultColor.RED
	local _ColorVol = DefaultColor.YELLOW
	local _ColorCur = nil
	local _winName = Label[1].."Background"
	if not DoesWindowExist(_winName) then return end
	if val > 0 then _ColorCur = _ColorPos end
	if val < 0 then _ColorCur = _ColorNeg end
	if Label[2] == "Total" and val == 0 and GG.tradeskill ~= GG.C.apot then _ColorCur = _ColorNeg end
	if Label[2] == "TotalB" and val == 0 then _ColorCur = _ColorNeg end
	if Label[2] == "TotalB" and val == 1 then _ColorCur = _ColorVol end --stablity 1 => yellow
	if _ColorCur then 
		WindowSetTintColor(_winName, _ColorCur.r, _ColorCur.g, _ColorCur.b)
		WindowSetAlpha(_winName, 0.5)
	else
		WindowSetAlpha(_winName, 0)
	end
end

----------[Item data collection functions]----------------------------------------------
function GG.ExportResults()
	local _Keys = GG.CollectKeys(GG.S["Results"], -2)
	GG.Debug(2, _Keys, "CollectKeys") --because function is recursive
	local _Export = GG.CreateExport(GG.S["Results"], _Keys, "results")
	GG.ExportData(_Export, _Keys, GG.C.eresuFilename)
	return #_Export
end

function GG.ExportItems(Items)
	local _Keys = GG.CollectKeys(Items, -2)
	GG.Debug(2, _Keys, "CollectKeys") --because function is recursive
	local _Export = GG.CreateExport(Items, _Keys, "items")
	GG.ExportData(_Export, _Keys, GG.C.eitemFilename)
	GG.doExport = false
	return #_Export
end

function GG.ExportData(Source, Keys, filename)
	local _sep = ";"
	local _keys = nil
	for k, v in pairs(Keys) do
		if _keys then _keys = _keys.._sep..k
		else _keys = k
		end
	end
	local _filename = filename
	TextLogCreate(_filename, 1)
	TextLogSetEnabled(_filename, true)
	TextLogClear(_filename)
	--TextLogSetIncrementalSaving(_filename, true, "logs\\".._filename)
	TextLogAddEntry(_filename, nil, towstring(_keys))
	for k, v in ipairs(Source) do
		local _string = GG.TableToString(v, _sep)
		TextLogAddEntry(_filename, nil, towstring(_string))
	end
	TextLogDestroy(_filename)
	GG.Debug(2, "exported", "ExportText")
end

function GG.TableToString(Source, separator)
	if not Source then return end
	local _T = {}
	for k, v in pairs(Source) do --pairs
		local _val = v
		if type(v) == "table" then _val = ""
		elseif type(v) == "function" then _val = ""
		elseif type(v) == "wstring" then _val = tostring(v)
		elseif type(v) == "boolean" and v then _val = "true"
		elseif type(v) == "boolean" and not v then _val = "false"
		end
		table.insert(_T, _val)
	end
	return table.concat(_T, separator)
end

function GG.CollectKeys(Source, level, path)
	local _sep = "|"
	path = path or ""
	level = level or 0
	level = level + 1
	if level > 3 then return end
	local _T, _Sub = {}, {}
	for k,v in pairs(Source) do
		if type(v) == "table" then
			if level >= 0 then _Sub = GG.CollectKeys(v, level, path..k.._sep)
			else _Sub = GG.CollectKeys(v, level)
			end
			if _Sub then
				for kSub, vSub in pairs(_Sub) do
					_T[kSub] = 1
				end
			end
		elseif type(v) ~= "function" and level >= 0 then
		 _T[path..k] = 1
		end
	end
	return _T
end

function GG.CreateExport(Source, Keys, exType)
	exType = exType or "items"
	local _T = {}
	for k, v in pairs(Source) do
		local _col = 0
		table.insert(_T, {})
		for kKeys, vKeys in pairs(Keys) do
			local _val = nil
			_col = _col + 1
			if exType == "items" then
				--use type() to catch boolean == false
				if kKeys:find("^Z|.+$") then
					local _key = kKeys:match("^Z|(.+)$")
					if tonumber(_key) then _key = tonumber(_key) end
					if type(v["Z"][_key]) then _val =	v["Z"][_key] end
				else
					if tonumber(kKeys) then kKeys = tonumber(kKeys) end
					if type(v[kKeys]) then _val =	v[kKeys] end
				end
			end
			if exType == "results" then
				--use type() to catch boolean == false
				local _k1, _k2, _k3 = kKeys:match("^(%w-)|(%d)|(.+)$")
				if tonumber(_k2) then _k2 = tonumber(_k2) end
				if tonumber(_k3) then _k3 = tonumber(_k3) end
				_val = v[_k1][_k2][_k3]
			end
			if _val == nil then	_val = ""	end
			if type(_val) == "wstring" then _val = tostring(_val) end
			_T[#_T][_col] = _val
		end
	end
	GG.Debug(2, _T, "CreateExport")
	return _T
end

function GG.SaveItems(ItemData)
	local _counter = 0
	for i=1, GG.C.sourceNum do
		if not ItemData[i] then continue end
		for k, v in ipairs(ItemData[i]) do
			if not v.uniqueID or v.uniqueID == 0 then continue end
			if GG.S["Items"][v.uniqueID] then continue end
			if not v["craftingBonus"] then continue end
			if not v["craftingBonus"][1] then continue end
			_counter = _counter + 1
			GG.S["Items"][v.uniqueID] = DataUtils.CopyTable(v)
		end
	end
	GG.Debug(2, _counter, "_counter-SaveItems")
	GG.Debug(3, GG.S["Items"], "SaveItems")
	return _counter
end

----------[Item data functions]----------------------------------------------
--Gather data from different sources to single table.
function GG.GetItemData(Source)
	local _T = {}
	for k, v in ipairs(Source) do
		if v == GG.C.sourceBack then _T[GG.C.sourceBack] = DataUtils.GetItems()
		elseif v == GG.C.sourceCraf then _T[GG.C.sourceCraf] = DataUtils.GetCraftingItems() --v1.3
		elseif v == GG.C.sourceBank then _T[GG.C.sourceBank] = DataUtils.GetBankData()
		elseif v == GG.C.sourceData then
			_T[GG.C.sourceData] = {}
			for k, v in pairs(GG.S["Items"]) do
				table.insert(_T[GG.C.sourceData], DataUtils.CopyTable(v))
			end
		elseif v == GG.C.sourceAuct then
			local _AuctionData = AuctionListDataManager.GetCurrentWindowData()
			if not _AuctionData then continue end
			--to get identical structures with other sources we need to remove one table level
			_AuctionData = _AuctionData["searchResultsData"]
			_T[GG.C.sourceAuct] = {}
			for kAuction, vAuction in ipairs(_AuctionData) do
				table.insert(_T[GG.C.sourceAuct], vAuction["itemData"])
			end
		end
	end
	local _itemCount = 0
	for k, v in pairs(_T) do
		_itemCount = _itemCount + #v
	end
	GG.Debug(3, _itemCount, "GetItemData,_itemCount")
	GG.Debug(2, _T, "GetItemData")
	return _T, _itemCount
end

--[[
	All values from first level of itemData are gathered.
	All custom values are added to sub-table Z.
	Items without craftingbonuses are ignored.
  Crafting resource type is defined on this level.
--]]
function GG.GetItemValues(ItemData)
	local _T = {}
	for i=1, GG.C.sourceNum do
		if not ItemData[i] then continue end
		for kSlot, vItem in ipairs(ItemData[i]) do
			if not vItem["craftingBonus"] then continue end
			if not vItem["craftingBonus"][1] then continue end
			table.insert(_T, {})
			--default values
			for k, v in pairs(vItem) do
				if type(v) == "table" then continue end
				_T[#_T][k] = v
			end
			--custom values
			_T[#_T]["Z"] = _T[#_T]["Z"] or {}
			_T[#_T]["Z"] = GG.GetCraftingBonus( vItem["craftingBonus"])
			_T[#_T]["Z"].allbonus = GG.CreateAllbonus(_T[#_T]["Z"])
			_T[#_T]["Z"].source = i
			_T[#_T]["Z"].slot = kSlot
			_T[#_T]["Z"].sSlot = i..kSlot --to make it work in conversion
			if _T[#_T]["Z"][5] == GG.C.apot and _T[#_T]["Z"][8] == 2 then
				_T[#_T]["Z"].apotResult = _T[#_T]["Z"][6]
			end
			if _T[#_T]["Z"][5] == GG.C.tali then
				_T[#_T]["Z"].taliResult = _T[#_T]["Z"][6]
				if _T[#_T]["Z"][8] == 14 then	--fragment
					_T[#_T]["Z"].taliRarity = vItem.rarity
				end
			end
			if vItem.cultivationType and vItem.cultivationType ~= 0 then
				_T[#_T]["Z"].cType = vItem.cultivationType + 100
			else
				_T[#_T]["Z"].cType = _T[#_T]["Z"][8]
			end
		end
	end
	GG.Debug(2, _T, "GetItemValues")
	return _T
end

--[[
	CraftingBonuses are converted from this:
	CraftingBonus[1].bonusReference = 5
	CraftingBonus[1].bonusValue = 99
	To this:
	CraftingBonus[5] = 99
--]]
function GG.GetCraftingBonus(CraftingBonus)
	if not CraftingBonus then return end
	local _T = {}
	for k, v in ipairs(CraftingBonus) do
		if _T[v.bonusReference] then continue --can be multiple values for one reference, just take first
		else _T[v.bonusReference] = v.bonusValue
		end
	end
	GG.Debug(3, _T, "GetCraftingBonus")
	return _T
end	

function GG.CreateAllbonus(Bonuses)
	local _All = {}
	for k, v in pairs(Bonuses) do
		local _bonus = k..":"..v
		table.insert(_All, _bonus)
	end
	return table.concat(_All, ",")
end

function GG.FilterItems(Items, target, Ranges, rType, delTarget)
	local _T = {}
	delTarget = delTarget or false --true = exclude target, false = include only target
	rType = rType or 1 --1 = list of values, 2 = two value range
	for kItem, vItem in ipairs(Items) do
		local _found = false
		local _item = vItem[target]
		if vItem["Z"][target] then _item = vItem["Z"][target] end
		if rType == 1 then --list
			for k, v in ipairs(Ranges) do
				if _item == v then _found = true end
			end
		elseif rType == 2 then --range
			if _item >= Ranges[1] and _item <= Ranges[2] then _found = true end
		end
		if (delTarget and not _found) or (not delTarget and _found) then
			table.insert(_T, vItem)
		end
	end
	GG.Debug(2, _T, "FilterItems")
	return _T
end

----------[Resource list functions]----------------------------------------------
function GG.CreateCalculations(List)
	local _T = {}
	for iRow=1, GG.rowsMax do
		local _total = 0
		table.insert(_T, {})
		for kCol, vCol in pairs(List) do --pairs
			if vCol[iRow+1] then --+1 => ignore arg 1 (slot)
				_T[#_T][kCol] = vCol[iRow+1] --_T[row#][col#] = Item#[arg#]
				_total = _total + vCol[iRow+1]
			end
		end
		if iRow == GG.bonusRow then _total = _total + GG.GetBonus() end
		if iRow == GG.stabilityRow then _total = _total + GG.GetBonusStability() end
		_T[#_T][GG.C.resultCol] = _total
	end
	GG.Debug(2, _T, "CreateCalculations")
	return _T
end

function GG.PrintList(List, Args, itemCount)
	local _showCount = 0
	local _MsgList = {}
	for kItem, vItem in ipairs(List) do
		local _Msg = {}
		_showCount = _showCount + 1
		for kArg, vArg in ipairs(Args) do
			local _val = "_"
			if vItem[kArg] then _val = vItem[kArg] end
			table.insert(_Msg, _val)
		end
		table.insert(_MsgList, table.concat(_Msg, ","))
	end
	table.sort(_MsgList)
	GG.Debug(2, _MsgList, "PrintList")
	for k, v in ipairs(_MsgList) do
		EA_ChatWindow.Print(towstring(v))
	end
	EA_ChatWindow.Print(towstring("Items processed/showed: "..itemCount.."/".._showCount))
end

function GG.CreateList(Values, Format)
	local _T = {}
	for kItem, vItem in ipairs(Values) do
		table.insert(_T, {})
		for kArg, vArg in ipairs(Format) do
			local _val = nil
			--search first from custom then default
			if vItem["Z"][vArg[1]] then _val = vItem["Z"][vArg[1]]
			elseif vItem[vArg[1]] then _val = vItem[vArg[1]]
			end
			_T[#_T][kArg] = _val
		end
	end
	--return table format: _T[#].argnumber = value
	--only found values returned
	GG.Debug(2, _T, "CreateList")
	return _T
end


function GG.DoListConversions(List, Args)
	local _T = {}
	local _ResourceName = GG.T.ResConversion
	local _TaliName = GG.T.TaliResult
	
	for k, v in ipairs(List) do
		table.insert(_T, {})
		for kArg, vVal in pairs(v) do --pairs
			for kFormat, vFormat in ipairs(Args[kArg][2]) do
				if not vFormat then continue end
				if vFormat == 1 and tonumber(vVal) > 60000 then vVal = vVal - (256 * 256) end
				if vFormat == 2 and tonumber(vVal) < 10 then	vVal = "0"..vVal end
				if vFormat == 3 and tonumber(vVal) < 10 then	vVal = "0"..vVal end
				if vFormat == 3 and tonumber(vVal) < 100 then	vVal = "0"..vVal end
				if vFormat == 4 and not _ResourceName[vVal] then vVal = "#unknown" end
				if vFormat == 4 and _ResourceName[vVal] then vVal = _ResourceName[vVal][1] end
				if vFormat == 5 then vVal = tostring(vVal) end
				if vFormat == 6 then vVal = GG.GetSlot(vVal) end
				if vFormat == 7 and not _ResourceName[vVal] then vVal = "#unknown" end
				if vFormat == 7 and _ResourceName[vVal] then vVal = _ResourceName[vVal][2] end
				if vFormat == 8 and not _TaliName[vVal] then vVal = "#unknown" end
				if vFormat == 8 and _TaliName[vVal] then vVal = tostring(_TaliName[vVal].name) end
				if vFormat == 9 and not GG.T.ApotDef[vVal] then vVal = "#unknown" end
				if vFormat == 9 and GG.T.ApotDef[vVal] then vVal = GG.T.ApotDef[vVal][3] end
			end
			_T[#_T][kArg] = vVal
		end
	end
	GG.Debug(2, _T, "DoListConversions")
	return _T
end

function GG.GetSlot(slot)
	local _step, _sou = 8, nil
	local _source, _slot = slot:match("(%d)(%d+)")
	_source, _slot = tonumber(_source), tonumber(_slot)
	if _source == GG.C.sourceAuct then _step = 6 end	--auction page length
	if _source == GG.C.sourceBack then _sou = "a" end
	if _source == GG.C.sourceBank then _sou = "b" end
	if _source == GG.C.sourceAuct then _sou = "c" end
	if _source == GG.C.sourceData then _sou = "d" end
	if _source == GG.C.sourceCraf then _sou = "e" end --v1.3
	local _row = math.floor(_slot / _step ) + 1
	local _col = math.mod(_slot, _step)
	if _col == 0 then
		_row = _row - 1 
		_col = _step
	end
	return _sou.._row..":".._col
end

----------[Hooks]----------------------------------------------
function GG.SetHooks()
	--Attached to event TalismanMakingWindow.OnShown
	GG.H1.TalismanMakingWindow_Clear = TalismanMakingWindow.Clear
	TalismanMakingWindow.Clear = GG.H2.TalismanMakingWindow_Clear
	--Attached to event TalismanMakingWindow.OnHidden
	GG.H1.TalismanMakingWindow_OnHidden = TalismanMakingWindow.OnHidden
	TalismanMakingWindow.OnHidden = GG.H2.TalismanMakingWindow_OnHidden

	--Attached to event ApothecaryWindow.OnShown
	GG.H1.ApothecaryWindow_OnShown = ApothecaryWindow.OnShown
	ApothecaryWindow.OnShown = GG.H2.ApothecaryWindow_OnShown
	--Attached to event ApothecaryWindow.OnHidden
	GG.H1.ApothecaryWindow_OnHidden = ApothecaryWindow.OnHidden
	ApothecaryWindow.OnHidden = GG.H2.ApothecaryWindow_OnHidden

	--This function seem to be called always when crafting data changes.
	--It also enables to catch data on wanted crafting states.
	GG.H1.CraftingSystem_SetState = CraftingSystem.SetState
	CraftingSystem.SetState = GG.H2.CraftingSystem_SetState
end

function GG.H2.TalismanMakingWindow_Clear(...)
	GG.H1.TalismanMakingWindow_Clear(...)
	GG.ShowWindows()
end

function GG.H2.TalismanMakingWindow_OnHidden(...)
	GG.H1.TalismanMakingWindow_OnHidden(...)
	GG.ShowWindows()
end

function GG.H2.ApothecaryWindow_OnShown(...)
	GG.H1.ApothecaryWindow_OnShown(...)
	GG.ShowWindows()
end

function GG.H2.ApothecaryWindow_OnHidden(...)
	GG.H1.ApothecaryWindow_OnHidden(...)
	GG.ShowWindows()
end

function GG.H2.CraftingSystem_SetState(...)
	GG.H1.CraftingSystem_SetState(...)
	GG.HelperWindowInfo()
end

----------[Optional dependencies]----------------------------------------------
function GG.OptionalDependencies(moduleName)
	local _modules = ModulesGetData()
	for k, v in ipairs(_modules) do
		if v.name == moduleName then
			if v.isEnabled and not v.isLoaded then ModuleInitialize(v.name)	end
			return true
		end
	end
	return false
end

----------[EventHandlers]----------------------------------------------
function GG.PLAYER_INVENTORY_SLOT_UPDATED(Slot)
	GG.Debug(2, Slot, "GG.PLAYER_INVENTORY_SLOT_UPDATED")
	GG.lastSlotUpdated = Slot[1]
end

----------[Debug functions]----------------------------------------------
function GG.Debug(level, Source, name)
	if not GG.debugMode then return end
	level = level or 1
	if GG.debugLevel < level then return end
	table.insert(GG.D, {})
	if type(Source) == "table" then
		GG.D[#GG.D][name] = DataUtils.CopyTable(Source)
	else
		GG.D[#GG.D]["--"..name] = {Source}
	end
end

----------[Parameter initializations]----------------------------------------------
function GG.GetTaliArmorBonus(power, rarity)
	power = power + (3 * rarity)
	if power == 2 then return 15 end
	if power == 3 then return 23 end
	if power == 4 then return 30 end
	if power == 5 then return 38 end
	if power == 6 then return 45 end
	if power == 7 then return 53 end
	if power == 8 then return 60 end
	if power == 9 then return 68 end
	if power == 10 then return 75 end
	if power == 11 then return 83 end
	if power == 12 then return 90 end
	if power == 13 then return 98 end
	if power == 14 then return 105 end
	return 0
end

function GG.GetTaliLevel(power, previous)
	if GG.tradeskill ~= GG.C.tali then return 0 end
	local _powerTotal = power
	local _level = 0
	local _need = 0
	if GG.T["Custom"][1] == 0 then _powerTotal = _powerTotal + GG.T["Current"][3] end
	if _powerTotal < 0 then return 0 end
	for k, v in pairs(GG.T.TaliDef) do
		if _powerTotal <= v.max then _level = k break end
	end
	if previous and _level == 1 then return 0 end
	if previous then
		_need = (_powerTotal - GG.T.TaliDef[_level-1].max) * -1		--L2(9-11) -> 8
	else
		_need = GG.T.TaliDef[_level].max - _powerTotal + 1		--L2(9-11) -> 12
	end
	return _need
end

function GG.GetTaliLevelReq(powerLevel)
  if powerLevel <= 1 then return 1 end
  if powerLevel <= 3 then return 6 end
  if powerLevel <= 5 then return 11 end
  if powerLevel <= 7 then return 21 end
  if powerLevel <= 9 then return 31 end
  if powerLevel <= 10 then return 36 end
  return 0
end

function GG.GetTaliDuration(powerLevel)
  local _day = 24 * 3600
  if powerLevel <= 6 then return powerLevel * _day end
  if powerLevel <= 7 then return 9 * _day end
  if powerLevel <= 8 then return 10 * _day end
  if powerLevel <= 9 then return 13 * _day end
  if powerLevel <= 10 then return 14 * _day end
  return 0
end

function GG.GetApotLevel(power, previous)
	if GG.tradeskill ~= GG.C.apot then return 0 end
	if previous then
			if power == 1 then return -1 end
			for i=(power-1), 0, -1 do
			if GG.T.ApotDef[i] then return -(power - i) end
		end
	else
		for i=(power+1), GG.typeMax do
			if GG.T.ApotDef[i] then return (i - power) end
		end
	end
	return power
end

function GG.GetBonusStability()
	local _Bonus = GG.T.ApotBonusStability
	local _skill = GameData.CraftingStatus.SkillLevel
	if GG.tradeskill == GG.C.tali then return 0 end
	for k, v in ipairs(_Bonus) do
		if _skill <= v.max then return v.bonus end
	end
	return 0
end

function GG.GetBonus()
	local _Bonus = GG.T.ApotBonus
	local _skill = GameData.CraftingStatus.SkillLevel
	if GG.tradeskill == GG.C.tali then _Bonus = GG.T.TaliBonus end
	for k, v in ipairs(_Bonus) do
		if _skill <= v.max then return v.bonus end
	end
	return 0
end

function GG.GetApotResult(resourceNum, pName)
	for k, v in ipairs(GG.T.ApotResult) do
		if resourceNum == v.link then
			if pName then return v.name end
			if not pName then return k end
		end
	end
	return 0
end

function GG.SetApotConversion()
	local _meleeP = tostring(GetString(StringTables.Default.LABEL_BONUS_DAMAGE_MELEE))
	local _magicP = tostring(GetString(StringTables.Default.LABEL_BONUS_DAMAGE_MAGIC))
	local _rangedP = tostring(GetString(StringTables.Default.LABEL_BONUS_DAMAGE_RANGED))
	local _meleeC = tostring(GetString(StringTables.Default.LABEL_BONUS_CRITICAL_HIT_RATE_MELEE))
	local _magicC = tostring(GetString(StringTables.Default.LABEL_BONUS_CRITICAL_HIT_RATE_MAGIC))
	local _rangedC = tostring(GetString(StringTables.Default.LABEL_BONUS_CRITICAL_HIT_RATE_RANGED))
	local _healP = tostring(GetString(StringTables.Default.LABEL_BONUS_HEALING_POWER))
	local _T = { --[stat6(type)] = {start, pot-type(1=normal,2=heal/ap,3=new1.2pots), title} 
		[1] = {7871+1, 2, "Heal"},
		[2] = {7879, 1, "HoT"},
		[3] = {7889+1, 2, "AP"},
		[4] = {7892+(1*5), 1, tostring(StatInfo[1].name)}, --str
		[5] = {7892+(2*5), 1, tostring(StatInfo[9].name)}, --int
		[6] = {7892+(14*5), 1, tostring(StatInfo[3].name)}, --wil
		[7] = {7892+(4*5), 1, tostring(StatInfo[4].name)}, --tou
		[8] = {7892+(3*5), 1, tostring(StatInfo[8].name)}, --bal
		[9] = {7892+(0*5), 1, "AbsorbShield"},
		[10] = {7892+(7*5), 1, tostring(StatInfo[16].name)}, --Rco
		[11] = {7892+(6*5), 1, tostring(StatInfo[15].name)}, --Rel
		[12] = {7892+(5*5), 1, tostring(StatInfo[14].name)}, --Rsp
		[13] = {7892+(8*5), 1, "Armor"},
		[14] = {7892+(9*5), 1, "SpikeShield"},
		[15] = {7892+(10*5), 1, "DoT"},
		[16] = {7892+(11*5), 1, "AoE-DoT"},
		[17] = {7892+(13*5), 1, "Cone-DoT"},
		[18] = {7892+(12*5), 1, "Snare"},
		[21] = {15616+0, 3, tostring(StatInfo[4].name).."+".._meleeC}, --tou
		[22] = {15616+1, 3, tostring(StatInfo[1].name).."+".._meleeP}, --str
		[23] = {15616+2, 3, tostring(StatInfo[3].name).."+".._healP}, --wil
		[24] = {15616+3, 3, tostring(StatInfo[1].name).."+".._healP}, --str
		[25] = {15616+4, 3, tostring(StatInfo[9].name).."+".._magicC}, --int
		[26] = {15616+5, 3, tostring(StatInfo[8].name).."+".._rangedC}, --bal
		[27] = {15616+6, 3, tostring(StatInfo[5].name).."+".._meleeP}, --wou
		[28] = {15616+7, 3, tostring(StatInfo[5].name).."+".._meleeC}, --wou
		[29] = {15616+8, 3, tostring(StatInfo[5].name).."+".._magicC}, --wou
		[30] = {15616+9, 3, tostring(StatInfo[5].name).."+".._rangedC}, --wou
		[31] = {15616+10, 3, tostring(StatInfo[5].name).."+".._healP}, --wou
		[32] = {15616+11, 3, tostring(StatInfo[5].name).."+"..tostring(StatInfo[1].name)}, --wou+str
		[41] = {15977+0, 3, tostring(GetString( StringTables.Default.LABEL_BONUS_TRADE_SKILL_APOTHECARY ))},
		[42] = {15977+1, 3, tostring(GetString( StringTables.Default.LABEL_BONUS_TRADE_SKILL_CULTIVATION ))},
		[43] = {15977+2, 3, tostring(GetString( StringTables.Default.LABEL_BONUS_TRADE_SKILL_SALVAGING ))},
		[44] = {15977+3, 3, tostring(GetString( StringTables.Default.LABEL_BONUS_TRADE_SKILL_TALISMAN ))},
		[45] = {15977+4, 3, "Resurrection"},
		[46] = {15977+5, 3, tostring(GetString( StringTables.Default.LABEL_BONUS_MORALE_GEN ))},
	}
	return _T
end

function GG.GetApotMultiplier(value)
	value = value or 0
	if value <= 1 then return 1 end
	if value <= 3 then return 2 end
	if value <= 7 then return 3 end
	if value <= 15 then return 4 end
	if value > 15 then return 5 end
end

function GG.GetApotDuration(value)
	value = value or 0
	if value <= 1 then return 1 end
	if value <= 3 then return 2 end
	if value <= 7 then return 3 end
	if value <= 15 then return 4 end
	if value > 15 then return 5 end
end

function GG.GetApotPower(value, volatile)
	for k, v in ipairs(GG.T.ApotPow) do
		if value <= v.max then
			local _iLevel = v.level
			if volatile then _iLevel = v.vLevel end
			return v.rank, _iLevel, k --rank, iLevel, powerLevel
		end
	end
	return 0, 0, 0
end

function GG.GetApotReference(rType, powerLevel, duration, volatile)
	local _Resource = GG.T.ApotDef[rType]
	local _stepStart = _Resource[1]
	local _stepPow = 0
	local _stepDur = 0
	if volatile and _Resource[2] == 2 then _stepStart = _stepStart - 1	end --heal/ap & volatile
	if _Resource[2] == 1 then	_stepDur = duration - 1 end --normal pots (not new ones)
--	d("S,+P =+D: "..(_stepStart)..", "..(_stepStart + _stepPow).." = "..(_stepStart + _stepPow + _stepDur))
	return (_stepStart + _stepPow + _stepDur)
end

function GG.SetConversionTables()
	do --GG.T.Formats
		GG.T.Formats = {}
--[[
		Argument in slash command is converted to a value in this table.
		eg. /cwillard 1 5 => GG.T.Formats[5]
		Second argument is the format type for the value. Types are:
		1 = >60000 numbers to negative
		2 = <10 to 0#
		3 = <100 to 0##
		4 = # to GG.T.ResConversion[#][1] = type keyword
		5 = wstring to string
		6 = slot# to {source}{row}_{col}
		7 = # to GG.T.ResConversion[#][2] = type label
		8 = fragmentType to GG.T.TaliResult[#].name = resulting type
		9 = main ingredientType to GG.T.ApotResult[#].name = resulting type
--]]
		GG.T.Formats[1] = {"cType", {4}}
		GG.T.Formats[2] = {2, {3}}
		GG.T.Formats[3] = {1, {1}}
		GG.T.Formats[4] = {"sSlot", {6}}
		GG.T.Formats[5] = {"name", {5}}
		GG.T.Formats[6] = {"rarity", {}}
		GG.T.Formats[7] = {12, {}}
		GG.T.Formats[8] = {"allbonus", {}}
		GG.T.Formats[9] = {"iLevel", {}}
		GG.T.Formats[10] = {"uniqueID", {}}
		GG.T.Formats[100] = {1, {1}}
		GG.T.Formats[101] = {"slot", {}}
		GG.T.Formats[102] = {"cType", {7}}
		GG.T.Formats[103] = {"taliResult", {8}}
		GG.T.Formats[104] = {"apotResult", {9}}
		GG.T.Formats[202] = {2, {1}}
		GG.T.Formats[203] = {3, {1}}
		GG.T.Formats[204] = {4, {1}}
		GG.T.Formats[205] = {5, {1}}
		GG.T.Formats[206] = {6, {1}}
		GG.T.Formats[207] = {"taliRarity", {1}}
end	
	do --GG.T.ResConversion
--Label language should change automatically related to client language.
		GG.T.ResConversion = {}
		GG.T.ResConversion[1] = {[1] = "#stabilizer"} --Just to put something since it's so common
		for k, v in pairs(GameData.CraftingItemType) do
			if k == "MIN_CRAFTING_ITEM_NUM" then continue end
			if k == "MAX_CRAFTING_ITEM_NUM" then continue end
			local _label = CraftingSystem.GetResourceTypeName(v)
			GG.T.ResConversion[v] = {k, tostring(_label)}
		end
		for k, v in pairs(GameData.CultivationTypes) do
			if k == "NUM_TYPES" then continue end
			if k == "NONE" then continue end
			local _label = CultivationWindow.typeNames[v]
			GG.T.ResConversion[v+100] = {k, tostring(_label)}
		end
	end
	do --GG.T.TaliResult
--[[
	Couldn't find conversion table for talisman fragment type
	to result type. So here is an educated guess.
	Fragments craftingbonus type 12 contains value for result type
	Fragment[type 12] / 4 = val => floor(val) = statNumber => statNumber + 1
	eg. GG.T.TalismanStats[statNumber=9].name = "Strenght"
--]]	
		local _TaliOrder = {16,15,14,4,8,7,9,3,1,6,5}
		local _TaliOrderMerchant = {8,9,3,1}
		GG.T.TaliResult = {}
		for i=1, 4 do --first 4 is armor
			table.insert(GG.T.TaliResult, {link = 26, name = tostring(GetString( StringTables.Default.LABEL_ARMOR ))})
		end
		for k, v in ipairs(_TaliOrder) do
			for i=1, 4 do
				table.insert(GG.T.TaliResult, {link = v, name = tostring(StatInfo[v].name)})
			end
		end
		for k, v in ipairs(_TaliOrderMerchant) do
			table.insert(GG.T.TaliResult, {link = v, name = tostring(StatInfo[v].name)})
		end

		GG.T.TaliDef = {}
		GG.T.TaliDef[1] = {max=9}
		GG.T.TaliDef[2] = {max=14}
		GG.T.TaliDef[3] = {max=19}
		GG.T.TaliDef[4] = {max=25}
		GG.T.TaliDef[5] = {max=30}
		GG.T.TaliDef[6] = {max=35}
		GG.T.TaliDef[7] = {max=40}
		GG.T.TaliDef[8] = {max=46}
		GG.T.TaliDef[9] = {max=51}
		GG.T.TaliDef[10] = {max=999}

		GG.T.TaliBonus = {}
		GG.T.TaliBonus[1] = {max=199,bonus=0}
		GG.T.TaliBonus[2] = {max=999,bonus=1}
    
--[[ obsolete since patch 1.3
		GG.T.TaliDef = {}
		GG.T.TaliDef[1] = {max=10}
		GG.T.TaliDef[2] = {max=26}
		GG.T.TaliDef[3] = {max=42}
		GG.T.TaliDef[4] = {max=58}
		GG.T.TaliDef[5] = {max=74}
		GG.T.TaliDef[6] = {max=90}
		GG.T.TaliDef[7] = {max=106}
		GG.T.TaliDef[8] = {max=122}
		GG.T.TaliDef[9] = {max=146}
		GG.T.TaliDef[10] = {max=999}

		GG.T.TaliBonus = {}
		GG.T.TaliBonus[1] = {max=149,bonus=0}
		GG.T.TaliBonus[2] = {max=174,bonus=1}
		GG.T.TaliBonus[3] = {max=199,bonus=2}
		GG.T.TaliBonus[4] = {max=999,bonus=3}
--]]
--[[
		GG.T.TaliDef = {}
		GG.T.TaliDef[1] = {max=8}
		GG.T.TaliDef[2] = {max=11}
		GG.T.TaliDef[3] = {max=27}
		GG.T.TaliDef[4] = {max=43}
		GG.T.TaliDef[5] = {max=59}
		GG.T.TaliDef[6] = {max=75}
		GG.T.TaliDef[7] = {max=91}
		GG.T.TaliDef[8] = {max=107}
		GG.T.TaliDef[9] = {max=123}
		GG.T.TaliDef[10] = {max=147}
		GG.T.TaliDef[11] = {max=999}

		GG.T.TaliBonus = {}
		GG.T.TaliBonus[1] = {max=49,bonus=0}
		GG.T.TaliBonus[2] = {max=99,bonus=1}
		GG.T.TaliBonus[3] = {max=149,bonus=2}
		GG.T.TaliBonus[4] = {max=199,bonus=3}
		GG.T.TaliBonus[5] = {max=999,bonus=4}
--]]

	end
	
	do --GG.T.ApotDesc

		GG.T.ApotBonus = {}
		GG.T.ApotBonus[1] = {max=99,bonus=0}
		GG.T.ApotBonus[2] = {max=199,bonus=1}
		GG.T.ApotBonus[3] = {max=999,bonus=2}

		GG.T.ApotBonusStability = {}
		GG.T.ApotBonusStability[1] = {max=99,bonus=0}
		GG.T.ApotBonusStability[2] = {max=149,bonus=1}
		GG.T.ApotBonusStability[3] = {max=999,bonus=2}

		GG.T.ApotPow = {}
		GG.T.ApotPow[1] = {max=2, rank=1, level=4, vLevel=3}
		GG.T.ApotPow[2] = {max=4, rank=5, level=8, vLevel=6}
		GG.T.ApotPow[3] = {max=6, rank=10, level=12, vLevel=9}
		GG.T.ApotPow[4] = {max=8, rank=15, level=16, vLevel=12}
		GG.T.ApotPow[5] = {max=10, rank=20, level=20, vLevel=15}
		GG.T.ApotPow[6] = {max=12, rank=25, level=24, vLevel=18}
		GG.T.ApotPow[7] = {max=14, rank=30, level=28, vLevel=21}
		GG.T.ApotPow[8] = {max=16, rank=32, level=32, vLevel=24}
		GG.T.ApotPow[9] = {max=18, rank=35, level=36, vLevel=27}
		GG.T.ApotPow[10] = {max=20, rank=38, level=40, vLevel=30}
		GG.T.ApotPow[11] = {max=99, rank=40, level=44, vLevel=33}
		
		GG.T.ApotDef = GG.SetApotConversion()
	end
	
--[[
	Thanks for Irinia for these conversions borrowed from CraftValueTip.
	GG.T.ApotResult = {}
	GG.T.ApotResult[1] = "Heal"
	GG.T.ApotResult[2] = "Armor"
	GG.T.ApotResult[3] = "Thorn Shield"
	GG.T.ApotResult[4] = "Intelligence"
	GG.T.ApotResult[5] = "Toughness"
	GG.T.ApotResult[6] = "Strength"
	GG.T.ApotResult[7] = "Willpower"
	GG.T.ApotResult[8] = "Accuracy"
	GG.T.ApotResult[9] = "Energy"
	GG.T.ApotResult[10] = "Restoration"
	GG.T.ApotResult[11] = "Absorb Shield"
	GG.T.ApotResult[12] = "Firey"
	GG.T.ApotResult[13] = "Firey Explosion"
	GG.T.ApotResult[14] = "Fire Breath"
	GG.T.ApotResult[15] = "Snare"
	GG.T.ApotResult[16] = "Corporeal Resist"
	GG.T.ApotResult[17] = "Elemental Resist"
	GG.T.ApotResult[18] = "Spirit Resist"
	GG.T.ApotResult[51] = "Yellow"
	GG.T.ApotResult[52] = "Brown"
	GG.T.ApotResult[53] = "Green"
	GG.T.ApotResult[54] = "Cyan"
	GG.T.ApotResult[55] = "Red"
--]]	

end

function GG.Reset()
	CraftingWillardSavedVariables = {}
	CraftingWillardSavedVariables["Items"] = {}
	CraftingWillardSavedVariables["Results"] = {}
	CraftingWillardSavedVariables.collectResults = false
	CraftingWillardSavedVariables.collectItems = false
	CraftingWillardSavedVariables.customAnchor = false
	CraftingWillardSavedVariables.customLayer = false
end
function GG.SetLabelTemplate()
	local _T = {}
	local _A = {24,  "LabelR", "Title"}
	local _B = {40/5,  "LabelX", "Value"}
	local _C = {5,  "LabelC", "Equal"}
	local _D = {13,  "LabelX", "Total"}
	local _D2 = {13,  "LabelX", "TotalB"} --stablity 1 => yellow
	local _E = {18,  "Counter", "Counter"}
	local _F = {41,  "LabelC", "Type"}
	local _G = {41,  "LabelC", "Rarity"}
	local _WindthLabel1 = { _A, _B, _B, _B, _B, _B, _C, _D, _E } --total = 100%
	local _WindthLabel2 = { _A, _B, _B, _B, _B, _B, _C, _D2, _E } --total = 100%
	local _WindthLabel3 = { _F, _F, _E} --total = 100%
	local _WindthLabel4 = { _G, _G, _E} --total = 100%
	
	if GG.tradeskill == GG.C.apot then
		table.insert(_T, _WindthLabel3)
		table.insert(_T, _WindthLabel1)
		table.insert(_T, _WindthLabel1)
		table.insert(_T, _WindthLabel1)
		table.insert(_T, _WindthLabel2)
	elseif GG.tradeskill == GG.C.tali then
		table.insert(_T, _WindthLabel3)
		table.insert(_T, _WindthLabel4)
		table.insert(_T, _WindthLabel1)
	end
	GG.Debug(1, _T, "CreateLabelTemplate")
	return _T
end

function GG.SetEditbox()
	local _T = {}
	if GG.tradeskill == GG.C.apot then
		table.insert(_T, {min=0, max=46, step1=1, step2=46 })
		table.insert(_T, {min=-23, max=23, step1=1, step2=5 })
		table.insert(_T, {min=-20, max=20, step1=1, step2=5 })
		table.insert(_T, {min=-20, max=20, step1=1, step2=5 })
		table.insert(_T, {min=-30, max=30, step1=1, step2=5 })
	elseif GG.tradeskill == GG.C.tali then
		table.insert(_T, {min=0, max=52, step1=1, step2=4 })
		table.insert(_T, {min=-5, max=5, step1=1, step2=99 })
		table.insert(_T, {min=-200, max=200, step1=1, step2=10 })
	end
	return _T
end

function GG.SetParameters(tradeskill)
	if tradeskill == GG.C.apot then
		GG.tradeskill = GG.C.apot
		GG.winMain = GG.C.WinName[GG.C.apot]
		GG.winNew = GG.C.WinName[GG.C.apotNew]
		GG.winTip1 = GG.C.WinName[GG.C.apotTip1]
		GG.winTip2 = GG.C.WinName[GG.C.apotTip2]
		GG.typeMax = 46
		GG.rowsMax = 5
		GG.bonusRow = 2
		GG.stabilityRow = 5
		GG.args = "101,206,202,203,204,100"
		GG.T.LabelTitle = {}
		table.insert(GG.T.LabelTitle, "CraftingWillard 2.0")
		table.insert(GG.T.LabelTitle, "Power:")
		table.insert(GG.T.LabelTitle, "Duration:")
		table.insert(GG.T.LabelTitle, "Multiplier:")
		table.insert(GG.T.LabelTitle, "Stability:")
	elseif tradeskill == GG.C.tali then
		GG.tradeskill = GG.C.tali
		GG.winMain = GG.C.WinName[GG.C.tali]
		GG.winNew = GG.C.WinName[GG.C.taliNew]
		GG.winTip1 = GG.C.WinName[GG.C.taliTip1]
		GG.winTip2 = GG.C.WinName[GG.C.taliTip2]
		GG.rowsMax = 3
		GG.bonusRow = 3
		GG.stabilityRow = 0
		GG.args = "101,206,207,202"
		GG.T.LabelTitle = {}
		table.insert(GG.T.LabelTitle, "CraftingWillard 2.0")
		table.insert(GG.T.LabelTitle, "")
		table.insert(GG.T.LabelTitle, "Power:")
	end
end
